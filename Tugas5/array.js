console.log("=====Soal 1=====")
console.log("")

function range(startNum, finishNum){
    var rangeArr = [];

    if ( startNum > finishNum) {
        var rangelength = startNum - finishNum + 1;
        for ( var i = 0; i< rangelength; i++){
            rangeArr.push(startNum - i)
        }
    }else if ( startNum < finishNum) {
        var rangelength = finishNum - startNum + 1;
        for (var i = 0; i < rangelength; i++){
            rangeArr.push(startNum + i)
        }
    }else if ( !startNum || finishNum) {
            return -1 
    }
    return rangeArr
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

console.log("")
console.log("===== Soal No.2=====")
console.log("")

function rangeWithStep(startNum, finishNum, step){
    var rangeArr = [];

    if ( startNum > finishNum) {
        var currentNum = startNum;
        for ( var i = 0; currentNum >= finishNum; i++){
            rangeArr.push(currentNum)
            currentNum -= step
        }
    }else if ( startNum < finishNum) {
        var currentNum = startNum
        var rangelength = finishNum - startNum + step;
        for (var i = 0; currentNum <= finishNum; i++){
            rangeArr.push(currentNum)
            currentNum -= step
        }
    }else if ( !startNum || finishNum || !step) {
            return -1 
    }
    return rangeArr
}

console.log(range(1,10)) 
console.log(range(5, 50, 2)) 
console.log(range(15,10)) 
console.log(range(20, 10, 2)) 
console.log(range(1)) 
console.log(range()) 

console.log("")
console.log("===== Soal No.3=====")
console.log("")

function sum(startNum, finishNum, step){
    var rangeArr = [];
    var range;

    if(!step){
        range=1
    }else{
        range = step
    }


    if ( startNum > finishNum){
        var currentNum = startNum;
        for (var i = 0; currentNum >= finishNum; i++){
            rangeArr.push(currentNum)
            currentNum -= range
        }
    }else if ( startNum < finishNum){
        var currentNum = startNum;
        for (var i = 0; currentNum <= finishNum; i++){
            rangeArr.push(currentNum)
            currentNum += range 
        }
    }else if (!startNum && !finishNum && !step){
        return 0
    }else if(startNum){
        return startNum

    }var sum = 0 
    for (var i = 0; i < rangeArr.length; i++){
        sum = sum + rangeArr[i]
    }
    return sum
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0

console.log("")
console.log("===== Soal No.4=====")
console.log("")

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

function dataHandling(data){
    var datalength = data.length
    for (var i=0; i<datalength; i++){
        var id = "Nomor ID: " + data[i][0]
        var nama = "Nama Lengkap: " + data[i][1]
        var TTL = "TTL: " + data[i][2]
        var Hobi = "Hobi: " + data[i][3]

        console.log(id)
        console.log(nama)
        console.log(TTL)
        console.log(Hobi)
    }
}

dataHandling(input)

console.log("")
console.log("===== Soal No.5=====")
console.log("")

function balikkata (kata){
    var katabaru = " ";
    for (var i = kata.length-1; i >= 0; i--){
        katabaru += kata[i]
    }
    return katabaru;
}

console.log("soal nomor 5")
console.log(balikkata("kasur"))
console.log(balikkata("SanberCode")) 
console.log(balikkata("Haji Ijah")) 
console.log(balikkata("racecar"))
console.log(balikkata("I am Sanbers")) 

console.log("")
console.log("===== Soal No.6=====")
console.log("")

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);

function dataHandling2(data){
    var newData = data
    var newName = data[1] + "Elsharawy" 
    var newProvince = "Provinsi " + data[2]
    var gender = "Pria"
    var Sekolah = "SMA Internasional Metro"

    newData.splice(1, 1, newName)
    newData.splice(2, 1, newProvince)
    newData.splice(4, 1, gender, Sekolah)

    var arrDate = data [3]
    var newDate = arrDate.split('/')
    var monthNum = newDate[1]
    var monthname = " "

    switch (monthNum){
    case "01":
        monthname = "Januari"
        break;
    case "02":
        monthname = "Februari"
        break;
    case "03":
        monthname = "Maret"
        break; 
    case "04":
        monthname = "April"
        break;
    case "05":
        monthname = "Mei"
        break;
    case "06":
        monthname = "Juni"
        break;
    case "07":
        monthname = "Juli"
        break;
    case "08":
        monthname = "Agustus"
        break; 
    case "09":
        monthname = "September"
        break;
    case "10":
        monthname = "Oktober"
        break;
    case "11":
        monthname = "November"
        break;
    case "12":
        monthname = "Desember"
        break;
    default:
        break;
    }

    var dateJoin =newDate.join("-")
    var dateArr = newDate.sort(function(value1, value2){
        value2- value1
    })
    var editName = newName.slice(0, 15)
    console.log(newData)

    console.log(monthname)
    console.log(dateArr)
    console.log(dateJoin)
    console.log(editName)
}

