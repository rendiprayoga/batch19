console.log("=====No. 1 Looping While=====")
console.log("LOOPING PERTAMA")
console.log("")
var iteration = true
var n = 2
var nlimit = 20

while(iteration){
    console.log(n," - ","I Love Coding")
    if(n >= nlimit){
        iteration = false
    } n+=2
}

console.log("LOOPING KEDUA")
var iteration = true
var n = 20
var nlimit = 2

while(iteration){
    console.log(n," - ","I will become a developer")
    if(n <= nlimit){
        iteration = false
    } n-=2
}

console.log("")
console.log("=====No. 2 Looping For=====")
console.log("OUTPUT")
console.log("")
for(var i = 1; i <= 20; i++) {
    if(i%2 == 0){
        console.log("berkualitas")
    }else{
        if(i%3 == 0){
            console.log("I Love Coding")
        }else console.log("santai")
    }
}

console.log("")
console.log("========Looping No. 3========")
console.log("")
for (var i = 1; i <= 4; i++) {
    console.log("####")
}

console.log("")
console.log("========Looping No. 4=========")
console.log("")
var ulangi =  true
var counter = 0;

while (ulangi) {
    counter++;
    var bintang = "#".repeat(counter);
    console.log(bintang);
    if(counter >= 7){
        ulangi = false
    };
}

console.log("")
console.log("========Looping No. 5=========")
console.log("")

for(var i = 1; i <= 8; i++) {
    if(i%2 == 0){
        console.log("# # # # ")
    }else{
        console.log(" # # # # ")
    }
}










