console.log("")
console.log("=====Mengubah function ke es6=====")
console.log("")


const golden =() =>{
    console.log("This is golden")
}

golden();

console.log("")
console.log("=====Sederhanakan objek literal di es6=====")
console.log("")

const newFunction = (firstName, lastName)=>{
    return{
        firstName,lastName,
        fullName(){
            console.log(firstName + " " + lastName)
            return
        }
    }
}
newFunction("William", "imoh").fullName()

console.log("")
console.log("=====no.3 Destructuring=====")
console.log("")

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }
const {firstName, lastName, destination, occupation, spell} = newObject

console.log(firstName, lastName, destination, occupation, spell)

console.log("")
console.log("=====no.4 Array Spreading=====")
console.log("")
