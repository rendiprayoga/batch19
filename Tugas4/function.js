console.log("======TUGAS HARI 4=====")
console.log("")
console.log("")
console.log("=====Tugas No.1=====")


function salam(){
     console.log("Halo Sanbers!")
}

 
console.log(salam()) // "Halo Sanbers!" 
console.log("")
console.log("=====Tugas NO.2=====")
console.log("")

function kalikan(){
   return num1*num2
}

 
var num1 = 12
var num2 = 4
 
var hasilKali = kalikan(num1, num2)
console.log(hasilKali) // 48

console.log("")
console.log("=====Tugas NO.3=====")
console.log("")


function introduce(){
    return console.log("Nama saya ", name, " umur saya ", age, "tahun, alamat saya di ", address, " dan saya punya hobi yaitu", hobby)
}
 
var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan) // Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat saya di Jln. Malioboro, Yogyakarta, dan saya punya hobby yaitu Gaming!" 
